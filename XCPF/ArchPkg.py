#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Copyright (C) 2016 Xyne
#
# This program is free software; you can redistribute it and/or
# modify it under the terms of the GNU General Public License
# (version 2) as published by the Free Software Foundation.
#
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA.

import calendar
import collections
import functools
import time
import urllib.error

from pyalpm import vercmp

import XCPF.common


#################################### PkgSet ####################################

class PkgSet(object):
  '''
  Sets of packages with associated operations such as unions and intersections.

  The accessors should be a dictionary of functions that accept a package as
  the sole argument and return the property identified by the key in the
  dictionary associated with that function. The required keys are:

  * name
  * version
  * groups

  '''
  def __init__(self, accessors, pkgs=None):
    self.accessors = accessors
    self.pkgs = dict()
    if pkgs:
      for pkg in pkgs:
        self.pkgs[self.accessors['name'](pkg)] = pkg

  def __repr__(self):
    return 'PkgSet({})'.format(repr(self.pkgs))

  def copy(self):
    return self.__class__(self)

  def clear(self):
    self.pkgs.clear()

  def add(self, pkg):
    self.pkgs[self.accessors['name'](pkg)] = pkg
    return self

  def addmany(self, pkgs):
    for pkg in pkgs:
      self.add(pkg)
    return self

  def __iadd__(self, other):
    return self.addmany(other)

  def __add__(self, other):
    copy = self.copy()
    return copy.__iadd__(other)

  def remove(self, pkg):
    if isinstance(pkg, str):
      name = pkg
    else:
      name = self.accessors['name'](pkg)
    try:
      del self.pkgs[name]
    except KeyError:
      pass
    return self

  def removemany(self, pkgs):
    for pkg in pkgs:
      self.remove(pkg)
    return self

  def __isub__(self, other):
    return self.removemany(other)

  def __add__(self, other):
    copy = self.copy()
    return copy.__isub__(other)

  def remove_version(self, name, version):
    try:
      if self.accessors['version'](self.pkgs[name]) == version:
        del self.pkgs[name]
    except KeyError:
      pass
    return self

  def names(self):
    return self.pkgs.keys()

  def ignore(self, names, groups):
    names = set(names)
    groups = set(groups)
    ignored = set()
    for name, pkg in self.pkgs.items():
      if names and name in names:
        ignored.add(name)
      else:
        try:
          grps = self.accessors['groups'](pkg)
        except KeyError:
          pass
        else:
          if (groups & set(grps)):
            ignored.add(name)
    for i in ignored:
      del self.pkgs[i]
    return self

  def __and__(self, other):
    copy = self.copy()
    return copy.__iand__(other)

  def __iand__(self, other):
    delenda = set(self.pkgs) - set(other.pkgs)
    for name in delenda:
      del self.pkgs[name]
    return self

  def __or__(self, other):
    copy = self.copy()
    return copy.__ior__(other)

  def __ior__(self, other):
    self.pkgs.update(other.pkgs)
    return self

  def __contains__(self, pkg):
    if isinstance(pkg, str):
      return pkg in self.pkgs
    else:
      return self.accessors['name'](pkg) in self.pkgs

  def __iter__(self):
    for v in self.pkgs.values():
      yield v

  def __len__(self):
    return len(self.pkgs)

  def __bool__(self):
    return bool(self.pkgs)



class PyalpmPkgSet(PkgSet):

  def __init__(self, pkgs=None):
    accessors = {
      'name' : lambda x: x.name,
      'version' : lambda x: x.version,
      'groups' : lambda x: x.groups
    }
    super(self.__class__, self).__init__(accessors, pkgs=pkgs)



class PlaceholderPkgSet(PkgSet):
  '''
  PkgSet that can be used as a placeholder where an empty set is expected and
  another PkgSet type cannot be used.
  '''
  def __init__(self, pkgs=None):
    def f(x):
      raise ValueError('packages cannot be assigned to {}'.format(self.__class__))
    accessors = {
      'name' : f,
      'version' : f,
      'groups' : f
    }
    super(self.__class__, self).__init__(accessors, pkgs=pkgs)



############################## Buildable Packages ##############################

class BuildablePkgError(Exception):
  '''Exceptions raised by methods of the BuildablePkg class.'''
  pass



class BuildablePkgSet(PkgSet):

  def __init__(self, pkgs=None):
    accessors = {
      'name' : lambda x: x.pkgname(),
      'version' : lambda x: x.version()
    }
    super(self.__class__, self).__init__(accessors, pkgs=pkgs)



def collect_pkgbases(pkgs):
  pkgbases = dict()
  for pkg in pkgs:
    try:
      pkgbases[pkg.pkgbase()].append(pkg)
    except KeyError:
      pkgbases[pkg.pkgbase()] = [pkg]
  return pkgbases



class BuildablePkgMapping(collections.abc.Mapping):
  '''
  Wrapper class to provide a mapping of BuildablePkg. This can be used in string
  interpolations for example.
  '''
  def __init__(self, pkg):
    self.pkg = pkg
    super(self.__class__, self).__init__()
    # Use the AUR RPC names as they are the most likely to be encountered by
    # regular users.
    self.map = {
      'Name':         self.pkg.pkgname,
      'PackageBase':  self.pkg.pkgbase,
      'Repository':   self.pkg.repo,
      'Version':      self.pkg.version,
      'Maintainers':  self.pkg.maintainers,
      'Depends':      self.pkg.deps,
      'MakeDepends':  self.pkg.makedeps,
      'CheckDepends': self.pkg.checkdeps,
      'LastModified': self.pkg.last_modified
    }

  def __getitem__(self, key):
    v = self.map[key]()
    if not isinstance(v, str):
      try:
        v = ' '.join(iter(v))
      except TypeError:
        pass
    return v


  def __iter__(self):
    for k in self.map:
      yield k

  def items(self):
    for k, v in self.map.items():
      yield k, v()


  def __len__(self):
    return len(self.map)



# TODO
# Add a method to generate PKGBUILD-downloading Bash commands which can be used
# instead of the pbget commands.
@functools.total_ordering
class BuildablePkg(object):
  '''
  A wrapper object around different package objects to faciliate the generation
  of build scripts.
  '''

  def __init__(self, arch):
    self.arch = arch

  def _id(self):
    return '{}-{}'.format(self.qualified_pkgname(), self.version())

  def __eq__(self, other):
    if isinstance(other, BuildablePkg):
      return self._id() == other._id()
    else:
      return self._id() == other

  def __lt__(self, other):
    if isinstance(other, BuildablePkg):
      name_cmp = cmp(self.qualified_pkgname(), other.qualified_pkgname())
      if name_cmp < 0:
        return True
      elif name_cmp == 0:
        return (vercmp(self.version(), other.version()) < 0)
      else:
        return False
    else:
      return self._id() < other


  def buildable(self):
    '''
    Return a boolean indicating if this package is buildable.
    '''
    return False

  def trusted(self):
    '''
    Return a boolean indicating if this package should be intrinsically trusted.
    '''
    return False

  def maintainers(self):
    '''
    Iterate over current maintainers.
    '''
    return NotImplemented

  def pkgname(self):
    '''
    Return the package name.
    '''
    return NotImplemented

  def version(self):
    '''
    Return the package version.
    '''
    return NotImplemented

  def pkgbase(self):
    '''
    Return the package base.
    '''
    return NotImplemented

  def repo(self):
    '''
    Return the package repository or a suitable origin identifier.
    '''
    return NotImplemented

  def qualified_pkgbase(self):
    '''
    Returned the pkgbase with the repo prefix.
    '''
    return '{}/{}'.format(self.repo(), self.pkgbase())

  def qualified_pkgname(self):
    '''
    Returned the pkgname with the repo prefix.
    '''
    return '{}/{}'.format(self.repo(), self.pkgname())

  def last_modified(self):
    '''
    Return the last modification time, in UNIX format.
    '''
    return NotImplemented

  def deps(self):
    '''
    Iterate over runtime dependencies.
    '''
    return NotImplemented

  def makedeps(self):
    '''
    Iterate over build dependencies.
    '''
    return NotImplemented

  def checkdeps(self):
    '''
    Iterate over check dependencies.
    '''
    return NotImplemented

  def alldeps(self):
    '''
    Iterate over all dependencies.
    '''
    for d in self.deps():
      yield d
    for d in self.makedeps():
      yield d
    for d in self.checkdeps():
      yield d



class BuildablePkgFactory(object):
  '''
  Generalized class to wrap packages in a BuildablePkg class.
  '''
  def pkg(self, pkg):
    '''
    Convert a package to a buildable package if possible.
    '''
    return NotImplemented

  def pkgs(self, pkgs, *args, **kwargs):
    '''
    Generator around pkg method.
    '''
    for pkg in pkgs:
      yield self.pkg(pkg, *args, **kwargs)




class OfficialBuildablePkgFactory(BuildablePkgFactory):
  '''
  Wrapper class to convert pyalpm packages to OfficialBuildablePkgs.

  arch: Target architecture.
  config: Pacman configuration file (XCPF.PacmanConfig)
  opi: XCPF.common.OfficialPkgInfo instance
  '''
  def __init__(self, arch, *args, opi=None, **kwargs):
    self.arch = arch
    if opi is None:
      opi = XCPF.common.OfficialPkgInfo(
        *args,
        arch=arch,
        **kwargs
      )
    self.opi = opi

  def pkg(self, pkg, pkginfo=None):
    return OfficialBuildablePkg(
      self.arch,
      pkg,
      pkginfo=pkginfo,
      get_pkginfo=self.opi.retrieve_pkginfo
    )



class OfficialBuildablePkg(BuildablePkg):

  def __init__(self, arch, pkg, pkginfo=None, get_pkginfo=None):
    super(self.__class__, self).__init__(arch)
    self.pkg = pkg
    self.pkginfo = pkginfo
    for repo, x, y, z in XCPF.common.ARCHLINUX_OFFICIAL_REPOS:
      if self.pkg.db.name == repo:
        self.official = True
        break
    else:
      self.official = False
    if get_pkginfo is None:
      get_pkginfo = XCPF.common.archlinux_org_pkg_info
    self.get_pkginfo_function = get_pkginfo

  # Lazy retrieval.
  def get_pkginfo(self):
    if self.pkginfo is None:
      try:
        self.pkginfo = self.get_pkginfo_function(
          self.pkg.db.name, self.pkg.arch, self.pkg.name
        )
      except urllib.error.HTTPError as e:
        raise BuildablePkgError(
          'failed to retrieve pkginfo for {}'.format(self.pkg.name),
          error=e
        )
    return self.pkginfo

  def buildable(self):
    return self.official

  def trusted(self):
    return True

  def maintainers(self):
    try:
      for m in self.get_pkginfo()['maintainers']:
        yield m
    except KeyError as e:
      raise BuildablePkgError(
        'KeyError for {}'.format(self.pkg.name),
        error=e
      )

  def pkgname(self):
    return self.pkg.name

  def version(self):
    try:
      info = self.get_pkginfo()
      return '{}-{}'.format(info['pkgver'], info['pkgrel'])
    except KeyError as e:
      raise BuildablePkgError(
        'KeyError for {}'.format(self.pkg.name),
        error=e
      )

  def pkgbase(self):
    try:
      return self.get_pkginfo()['pkgbase']
    except KeyError as e:
      raise BuildablePkgError(
        'KeyError for {}'.format(self.pkg.name),
        error=e
      )

  def repo(self):
    return self.pkg.db.name

  def last_modified(self):
    try:
      return calendar.timegm(time.strptime(
        self.get_pkginfo()['last_update'],
        XCPF.common.ARCHLINUX_ORG_JSON_TIME_FORMAT
      ))
    except KeyError as e:
      raise BuildablePkgError(
        'KeyError for {}'.format(self.pkg.name),
        error=e
      )
    except ValueError as e:
      raise BuildablePkgError(
        'failed to parse mtime for {}'.format(self.pkg.name),
        error=e
      )

  def deps(self):
    try:
      # The pkginfo will be used for building and may be newer than the database
      # information, so prefer it.
      for m in self.get_pkginfo()['depends']:
        yield m
    except KeyError as e:
      raise BuildablePkgError(
        'KeyError for {}'.format(self.pkg.name),
        error=e
      )

  def makedeps(self):
    # TODO
    # Determine a way to get the makedeps. This isn't a showstopper because the
    # repo makedeps are guaranteed to be available via makepkg -irs.
    return iter([])

  def checkdeps(self):
    # TODO
    # Determine a way to get the makedeps. This isn't a showstopper because the
    # repo makedeps are guaranteed to be available via makepkg -irs.
    return iter([])


################################ Build Ordering ################################

class DependencyGraphNode(object):
  '''
  Callable dependency graph node. Each call will propagate dependency levels to
  all nodes above this one in the graph.
  '''

  def __init__(self, graph, pkg):
    self.graph = graph
    self.pkg = pkg
    self.n = -1
    graph[pkg.pkgname()] = self
    self.deps = sorted(set(
      XCPF.common.split_version_requirement(d)[0] for d in pkg.alldeps()
    ))
    self.called = False

  def __call__(self, n):
    # Prevent circular recursion and redundant calls.
    if self.called or n <= self.n:
      return
    try:
      self.called = True
      self.n = n
      for d in self.deps:
        try:
          self.graph[d](n+1)
        except KeyError:
          pass
    finally:
      self.called = False

  def __str__(self):
    return '{}:{:d}'.format(self.pkg.pkgname(), self.n)

  def __repr__(self):
    return 'DependencyGraphNode({})'.format(self.__str__())



def determine_dependency_graph(pkgs):
  '''
  Build the dependency graph.
  '''
  graph = dict()
  for pkg in pkgs:
    DependencyGraphNode(graph, pkg)
  for v in graph.values():
    v(0)
  return graph



def determine_build_order(graph):
  # Include the name in the sort key for deterministic build order.
  for d in sorted(graph.values(), key=lambda x: (x.n, x.pkg.pkgname()), reverse=True):
    yield(d.pkg)








