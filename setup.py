#!/usr/bin/env python3

from distutils.core import setup
import time

setup(
  name='''XCPF''',
  version=time.strftime('%Y.%m.%d.%H.%M.%S', time.gmtime(1627514868)),
  description='''Xyne's common Pacman functions, for internal use.''',
  author='''Xyne''',
  author_email='''ac xunilhcra enyx, backwards''',
  url='''http://xyne.archlinux.ca/projects/python3-xcpf''',
  packages=['''XCPF'''],
)
